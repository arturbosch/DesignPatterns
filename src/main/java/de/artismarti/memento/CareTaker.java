package de.artismarti.memento;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Artur
 */
public class CareTaker {

	private final Map<String, Memento> states = new HashMap<String, Memento>();

	public void saveMemento(Memento memento, String state) {
		System.out.println("Saving state... " + state);
		states.put(state, memento);
	}

	public Memento getMemento(String state) {
		System.out.println("Loading state... " + state);
		return states.get(state);
	}

	public void clearStates() {
		System.out.println("Clearing all stored states...");
		states.clear();
	}
}
