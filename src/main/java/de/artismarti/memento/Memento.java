package de.artismarti.memento;

/**
 * @author Artur
 */
public class Memento {

	private String magicString;
	private int magicNumber;

	public Memento(String magicString, int magicNumber) {
		this.magicNumber = magicNumber;
		this.magicString = magicString;
	}

	public String getMagicString() {
		return magicString;
	}

	public int getMagicNumber() {
		return magicNumber;
	}
}
