package de.artismarti.memento;

/**
 * @author Artur
 */
public class Originator {

	private String magicString;
	private int magicNumber;

	private String lastState;
	CareTaker careTaker;

	public Originator(final String magicString, final int magicNumber, final CareTaker careTaker) {
		this.magicString = magicString;
		this.magicNumber = magicNumber;
		this.careTaker = careTaker;
		saveToMemento("INIT");
	}

	public void saveToMemento(final String state) {
		lastState = state;
		careTaker.saveMemento(new Memento(magicString, magicNumber), state);
	}

	public void undo() {
		setOriginatorState(lastState);
	}

	public void undo(String state) {
		setOriginatorState(state);
	}

	public void undoAll() {
		setOriginatorState("INIT");
		careTaker.clearStates();
	}

	private void setOriginatorState(final String state) {
		Memento memento = careTaker.getMemento(state);
		setMagicNumber(memento.getMagicNumber());
		setMagicString(memento.getMagicString());
	}

	public String getMagicString() {
		return magicString;
	}

	public int getMagicNumber() {
		return magicNumber;
	}

	public void setMagicString(final String magicString) {
		this.magicString = magicString;
	}

	public void setMagicNumber(final int magicNumber) {
		this.magicNumber = magicNumber;
	}

	@Override public String toString() {
		return "Originator{" +
						"magicString='" + getMagicString() + '\'' +
						", magicNumber=" + getMagicNumber() +
						'}';
	}
}
