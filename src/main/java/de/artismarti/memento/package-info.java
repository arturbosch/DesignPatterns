/**
 * Used to save and undo internal state of objects.
 *
 * @author Artur
 */
package de.artismarti.memento;
