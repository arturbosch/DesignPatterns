package de.artismarti.flyweight;

/**
 * @author Artur
 */
public class ConcreteFlyweightA implements Flyweight {

	public ConcreteFlyweightA() {
		System.out.println("Created flyweight A ...");
	}

	@Override public void operation() {
		System.out.println("Concrete flyweight A ...");
	}
}
