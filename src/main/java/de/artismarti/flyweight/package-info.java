/**
 * Used when different types of an object are needed and their creation is expensive.
 * Flyweight pattern initializes the objects lazy and allows to reuse them.
 *
 * @author Artur
 */
package de.artismarti.flyweight;
