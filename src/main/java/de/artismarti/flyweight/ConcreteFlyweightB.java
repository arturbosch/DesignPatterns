package de.artismarti.flyweight;

/**
 * @author Artur
 */
public class ConcreteFlyweightB implements Flyweight {

	public ConcreteFlyweightB() {
		System.out.println("Created flyweight B ...");
	}

	@Override public void operation() {
		System.out.println("Concrete flyweight B ...");
	}
}
