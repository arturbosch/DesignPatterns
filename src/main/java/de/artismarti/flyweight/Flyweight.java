package de.artismarti.flyweight;

/**
 * @author Artur
 */
public interface Flyweight {

	void operation();

}
