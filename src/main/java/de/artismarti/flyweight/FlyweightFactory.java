package de.artismarti.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Artur
 */
public class FlyweightFactory {

	private static Map<String, Flyweight> holder = new HashMap<>();

	private FlyweightFactory() {
	}

	public static synchronized Flyweight getFlyweight(String type) {
		Flyweight flyweight = holder.get(type);

		if (flyweight == null) {
			switch (type) {
				case "A":
					flyweight = new ConcreteFlyweightA();
					break;
				case "B":
					flyweight = new ConcreteFlyweightB();
					break;
			}
			holder.put(type, flyweight);
		}

		return flyweight;
	}
}
