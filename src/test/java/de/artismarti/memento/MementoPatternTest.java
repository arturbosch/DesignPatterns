package de.artismarti.memento;

import org.junit.Test;

/**
 * @author Artur
 */
public class MementoPatternTest {

	@Test
	public void test() {

		Originator originator = new Originator("magic", 42, new CareTaker());
		System.out.println("Default State: " + originator);

		originator.setMagicString("Hello");
		originator.setMagicNumber(1);
		System.out.println("State: " + originator);
		originator.saveToMemento("SAVE1");

		originator.undo();
		System.out.println("State after undo: " + originator);

		originator.setMagicString("World");
		originator.setMagicNumber(6);
		System.out.println("State: " + originator);
		originator.saveToMemento("SAVE2");

		originator.undo("SAVE2");
		System.out.println("State after undo: " + originator);

		originator.undoAll();
		System.out.println("State after undoAll: " + originator);
	}

}
