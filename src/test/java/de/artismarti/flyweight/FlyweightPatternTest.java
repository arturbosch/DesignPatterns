package de.artismarti.flyweight;

import org.junit.Test;

/**
 * @author Artur
 */
public class FlyweightPatternTest {

	@Test
	public void test() {
		Flyweight flyweightA = FlyweightFactory.getFlyweight("A");
		flyweightA.operation();
		Flyweight flyweightA2 = FlyweightFactory.getFlyweight("A");
		flyweightA2.operation();
		Flyweight flyweightB = FlyweightFactory.getFlyweight("B");
		flyweightB.operation();
		Flyweight flyweightB2 = FlyweightFactory.getFlyweight("B");
		flyweightB2.operation();

	}
}
